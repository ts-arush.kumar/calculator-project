import java.util.Scanner;
import java.util.Stack;

public class Calc {
    public static void main(String[] args) {

        double num1;
        double num2;
        int num;
        String operation;

        Scanner input = new Scanner(System.in);
        System.out.println("Press 1 for std and 2 for sci");
        num=input.nextInt();
        if(num==1) {

            System.out.println("please enter the first number");
            num1 = input.nextDouble();

            System.out.println("please enter the second number");
            num2 = input.nextDouble();

            Scanner op = new Scanner(System.in);

            System.out.println("Please enter operation");
            operation = op.next();
            StandardCalculator std = new StandardCalculator();


                switch (operation) {
                case "+":
                    System.out.println("your answer is" + std.add(num1, num2));
                    break;
                case "-":
                    System.out.println("your answer is" + std.sub(num1, num2));
                    break;
                case "*":
                    System.out.println("your answer is" + std.mul(num1, num2));
                    break;
                case "/":
                    System.out.println("your answer is" + std.div(num1, num2));
                    break;
                case "%":
                    System.out.println("your answer is" + std.mod(num2, num1));
                    break;
                default:
                    System.out.println("Invalid");
                    break;
            }
        }

        else{
            Scanner NumInput = new Scanner(System.in);
            double firstNum = 0;
            double secondNum =0;
            double result = 0;
            System.out.println("Enter first number: ");
            firstNum = NumInput.nextDouble();
            System.out.println("Enter operator: ");
            String value = NumInput.next();
if(value.equals("^") || value.equals("Root")){
    System.out.println("Enter second number: ");
    secondNum = NumInput.nextDouble();
}
            if (value.equals("sin") || value.equals("cos") || value.equals("tan") || value.equals("cot") || value.equals("^") || value.equals("Root")){

                ScientificCalculator sci = new ScientificCalculator();

                switch(value){
                    case "sin":
                        result = (sci.sn(firstNum));
                        break;
                    case "cos":
                        result = (sci.cs(firstNum));
                        break;
                    case "tan":
                        result = (sci.tn(firstNum));
                        break;
                    case "^":
                        result = (sci.pw(firstNum,secondNum));
                        break;
                    case "Root":
                        result = (sci.Root(firstNum,secondNum));
                        break;
                    default : System.out.println("Invalid");
                        break;
                }
                System.out.println(result);
            }
        }
    }
}

