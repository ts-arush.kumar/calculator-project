import java.util.Scanner;
public class ScientificCalculator {
    public double sn(double a){
        return Math.sin(a);
    }
    public double cs(double a){
        return Math.cos(a);
    }
    public double tn(double a){
        return Math.tan(a);
    }
    public double pw(double a ,double b){
        return Math.pow(a,b);
    }
    public double Root(double a ,double b){
        return Math.pow(a,1/b);
    }
}
