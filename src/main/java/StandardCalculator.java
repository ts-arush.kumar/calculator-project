public class StandardCalculator {
    public double add(double a, double b) {return a+b;}
    public double sub(double a, double b) {return a-b;}
    public double mul(double a, double b) {return a*b;}
    public double div(double a, double b) {return a/b;}
    public int mod(double a, double b) {return (int) (b%a);}
}
